WARNING!

This repo contains a collection of MS-DOS viruses. These shouldn't be
too dangerous today, and Windows Defender will have no mercy on them,
but still, do not use outside a virtual machine
